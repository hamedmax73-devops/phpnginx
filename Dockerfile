# Debian version buster, bullseye
# Use buster for PHP <= 7.3 and bullseye for PHP >= 7.4
ARG DEBIAN_VERSION

# You can use version 7.1, 7.2, 7.3, 7.4
ARG BUILD_PHP_VERSION

FROM php:${BUILD_PHP_VERSION}-fpm-${DEBIAN_VERSION}

ARG COMPOSER_VERSION=2.3.5
ARG DEBIAN_FRONTEND="noninteractive"
ARG BUILD_PHP_VERSION

LABEL Maintainer="Sorna Team"
LABEL Name="Debisn Slim Docherfile including Nginx, PHP-FPM"
LABEL Version="20220427"
LABEL TargetImageName="gitlab.com/hamedmax73-devops/phpnginx/php-nginx-fpm:${BUILD_PHP_VERSION}-composer-${COMPOSER_VERSION}"

RUN cp /usr/share/zoneinfo/Asia/Tehran /etc/localtime && \
    echo "Asia/Tehran" >  /etc/timezone && \
    echo "Asia/Tehran" >  /etc/TZ

RUN apt-get update && apt-get install -y --no-install-recommends \
    curl \
    cron \
    wget \
    gnupg2 \
    ca-certificates \
    lsb-release \
    apt-transport-https \
    autoconf \
    nginx \
    supervisor \
    nodejs \
    zlib1g-dev \
    libmcrypt-dev \
    zip unzip \
    rsyslog \
    xz-utils \
    g++ make \
    libpng-dev \
    libxpm-dev  \
    libjpeg-dev \
    libwebp-dev  \
    libgmp-dev \
    libpng-dev \
    libzip-dev \
    libmcrypt-dev \
    libonig-dev \
    libsodium-dev \
    libxml2-dev  \
    libfreetype6-dev \
    libcurl4-openssl-dev \
    libpq-dev \
    librabbitmq-dev\
    # Developer tools
    tmux vim \
    && apt-get clean \
    && apt-get autoremove --yes \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

# add non-root user
RUN mkdir -p /var/log/supervisor /etc/supervisor/conf.d/ && \
    mkdir -p /var/tmp/nginx && \
    usermod -u 1000 www-data && groupmod -g 1000 www-data && \
    chown -R www-data:www-data /var/tmp/nginx

RUN NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && pecl install grpc apcu protobuf redis amqp \
    && docker-php-ext-enable grpc apcu protobuf redis amqp && \
    if dpkg --compare-versions "${BUILD_PHP_VERSION}" "gt" "7.1"; then \
        pecl install mongodb && docker-php-ext-enable mongodb; \
    fi

RUN NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && if dpkg --compare-versions "${BUILD_PHP_VERSION}" "lt" "7.4"; then \
    docker-php-ext-configure gd \
        --with-gd \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
        --with-xpm-dir=/usr/include/ \
        --with-webp-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ ; \
    else \
    docker-php-ext-configure gd \
        --with-freetype \
        --with-xpm \
        --with-webp \
        --with-jpeg ; \
    fi    \
    && \
    docker-php-ext-install -j $(nproc) \
    bcmath \
    curl \
    gd \
    exif \
    gmp \
    intl \
    mbstring \
    pdo_mysql \
    opcache \
    pdo \
    pgsql \
    pdo_pgsql \
    soap \
    xml \
    xmlwriter \
    sockets \
    zip && \
    docker-php-ext-enable \
    bcmath \
    curl \
    gd \
    exif \
    gmp \
    intl \
    mbstring \
    pdo_mysql \
    opcache \
    pdo \
    pgsql \
    pdo_pgsql \
    amqp \
    soap \
    xml \
    xmlwriter \
    sockets \
    zip

RUN if dpkg --compare-versions "${BUILD_PHP_VERSION}" "lt" "8.0"; \
    then  \
    docker-php-ext-install json && docker-php-ext-enable json; \
    fi

# because Sodium is supported natively by PHP 7.2+
RUN if [ "${BUILD_PHP_VERSION}" = "7.1" ] ; \
    then  \
    pecl install libsodium && docker-php-ext-enable sodium; \
    elif dpkg --compare-versions $BUILD_PHP_VERSION "lt" "8.1";  \
    then \
    pecl install mcrypt && docker-php-ext-enable mcrypt; \
    fi

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions http gettext

RUN curl -sS https://getcomposer.org/installer | php -- \
    --version=${COMPOSER_VERSION} \
    --install-dir=/usr/bin --filename=composer

RUN apt-get update && apt-get install -y --no-install-recommends netcat git && apt-get clean \
    && apt-get autoremove --yes \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

# Install nodejs
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash && apt-get install -y --no-install-recommends \
    nodejs \
    && apt-get clean \
    && apt-get autoremove --yes \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

# give permission to required path to the generated non-root
RUN mkdir -p /var/run/php/ \
    && mkdir -p /var/cache/nginx/ \
    && chown www-data:www-data /run/ -R \
    && chown www-data:www-data /var/ -R \
    && echo '<?php phpinfo(); ?>' > /var/www/index.php

COPY supervisord.conf /etc/supervisor/supervisord.conf