# nginx-php-fpm

## About

Nginx + PHP-FPM Docker image by [Samuraee](https://github.com/samuraee).

Compatible for Laravel concepts


All processes through this container handled by using Supervisord.
You can deploy every service by customizing supervisor config files like what you can see in container folder

## Usage
Arguments:
```
- NONROOT_USER         eg, scorpion
- DEBIAN_VERSION       eg, buster, bullseye
- PHP_VERSION          eg, 7.1, 7.2, 7..3, 7.4
- COMPOSER_VERSION     eg, 1.10.22, 2.1.6, stable
```

## Enviroment variables:
```
- TZ                   eg: Asia/Tehran
```
##  Build PHP base image

Available Debian versions: buster, bullseye
 - Use buster for PHP <= 7.3 and bullseye for PHP >= 7.4

Available PHP versions: 7.1, 7.2, 7.3, 7.4
```
docker build --build-arg PHP_VERSION=7.1 \
    --build-arg DEBIAN_VERSION=buster \
    --build-arg COMPOSER_VERSION=1.10.22 \
    --build-arg NONROOT_USER=scorpion \
    -f Dockerfile \
    -t registry.snapp.ninja/common-images/php/fpm-nginx-base:7.1-composer-1.10.22 .
```

Or you can build a multi-arch image using buildx:
```
docker buildx build --platform linux/arm64/v8,linux/amd64 --build-arg BUILD_PHP_VERSION=7.2 \
    --build-arg DEBIAN_VERSION=buster \
    --build-arg COMPOSER_VERSION=2.3.5 \
    --build-arg NONROOT_USER=application \
    -f php.Dockerfile \
    -t registry.snapp.ninja/common-images/php/fpm-nginx-base:7.2-composer-2.3.5 --push .
```
> Please instead of building multiple arch using single node, use multiple nodes with different architecture in order to experience better performance while building
# Run final application container

## Config files:
First of fill the folloewing files based on your desired configs
```
/etc/nginx/nginx.conf
/etc/nginx/sites-enabled/default
# php-fpm config
/usr/local/etc/php-fpm.d/www.conf

# php modules
/usr/local/etc/php/conf.d/modules.conf
/etc/supervisord.d/web-px.ini    # for web container
/etc/supervisord.d/cron-px.ini   # for cron container
/etc/supervisord.d/queue-px.ini  # for queue container
```

Ideally the above ones should be mounted from docker host
and container nginx configuration (see vhost.conf for example),
site files and place to right logs to.


## Sample laravel app Dockerfile (web contaner) with PHP 7.1

See `example-web.Dockerfile`

## Sample laravel queue Dockerfile (queue contaner) with PHP 7.1

See `example-queue.Dockerfile`

## Sample laravel cron Dockerfile (cron contaner) with PHP 7.1

See `example-cron.Dockerfile`